import { Inject, Injectable } from '@angular/core';
import { IUserDTO } from '../interfaces/user.interface';

@Injectable({
  providedIn: 'root',
})
export class UserDTO {
  data: Omit<IUserDTO, 'id' | 'first_name' | 'last_name' | 'avatar'>;

  constructor(
    @Inject('IUserDTO')
    data: Omit<IUserDTO, 'id' | 'first_name' | 'last_name' | 'avatar'>
  ) {
    this.data = data;
  }
}
