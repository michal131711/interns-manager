import { IUserDTO } from '../interfaces/user.interface';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class UserModel {
  data: IUserDTO[] = [];
  page: number = 0;
  per_page: number = 0;
  total: number = 0;
  total_pages: number = 0;
}
