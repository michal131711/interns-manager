export interface IUserDTO {
  id: number;
  first_name: string;
  last_name: string;
  avatar: string;
}
