import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AddInternComponent } from '../../shared/components/interns/add/add-intern.component';
import { EditInternComponent } from '../../shared/components/interns/edit/edit-intern.component';
import { FormInternComponent } from './interns/form-intern/form-intern.component';
import { ListInternComponent } from './interns/list/list-intern.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { SearchPipe } from '../pipes/search.pipe';
import { UsersListResolver } from '../resolvers/users-list.resolver';
import { UserDetailsResolver } from '../resolvers/user-details.resolver';
import { CarouselComponent } from '../../shared/components/carousel/carousel.component';

@NgModule({
  providers: [UsersListResolver, UserDetailsResolver],
  declarations: [
    ListInternComponent,
    AddInternComponent,
    EditInternComponent,
    FormInternComponent,
    CarouselComponent,
    SearchPipe,
  ],
  imports: [CommonModule, RouterModule, FormsModule, ReactiveFormsModule],
  exports: [CommonModule],
})
export class ComponentsModule {}
