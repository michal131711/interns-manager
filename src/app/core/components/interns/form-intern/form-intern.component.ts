import { Component, ElementRef, Input, OnInit, ViewChild } from '@angular/core';
import { UserService } from '../../../services/user.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { UserDTO } from '../../../dtos/UserDTO';
import { IUserDTO } from '../../../interfaces/user.interface';

@Component({
  selector: 'app-form-intern',
  templateUrl: './form-intern.component.html',
  styleUrls: ['./form-intern.component.scss'],
})
export class FormInternComponent implements OnInit {
  @ViewChild('avatar') avatar!: ElementRef;
  @Input() getUserData: UserDTO;
  form!: FormGroup;
  imgUrl: string | ArrayBuffer | null = '';

  constructor(
    private userService: UserService,
    private formBuilder: FormBuilder
  ) {}

  ngOnInit() {
    if (this.getUserData) {
      let getData: any = this.getUserData;
      let getDataUser: IUserDTO = getData.data;

      this.form = this.formBuilder.group({
        first_name: [getDataUser.first_name, [Validators.required]],
        last_name: [getDataUser.last_name, [Validators.required]],
        avatar: [getDataUser.avatar || this.imgUrl, [Validators.required]],
      });

      this.imgUrl = getDataUser.avatar || '';
    } else {
      this.form = this.formBuilder.group({
        first_name: ['', [Validators.required]],
        last_name: ['', [Validators.required]],
        avatar: ['', [Validators.required]],
      });
    }
  }

  uploadImageThumbnail(event: any): void {
    const file = event.target.files[0];
    const reader = new FileReader();

    reader.onload = () => {
      this.imgUrl = reader.result;
    };

    reader.readAsDataURL(file);
  }

  selectedPhoto(): void {
    this.avatar.nativeElement.click();
  }

  alert(message: string) {
    alert(message);
  }
}
