import { Component, OnDestroy, OnInit } from '@angular/core';
import { UserService } from '../../../services/user.service';
import { Subject, Subscription, takeUntil } from 'rxjs';
import { UserModel } from '../../../models/User.model';
import { IUserDTO } from '../../../interfaces/user.interface';
import { UserDTO } from '../../../dtos/UserDTO';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-list-intern',
  templateUrl: 'list-intern.component.html',
  styleUrls: ['list-intern.component.scss'],
})
export class ListInternComponent implements OnInit, OnDestroy {
  private ngUnsubscribe = new Subject<UserDTO>();
  listingUsers!: UserModel;
  findUsers!: string;

  constructor(
    private userService: UserService,
    private route: ActivatedRoute
  ) {}

  ngOnInit(): void {
    this.route.data
      .pipe(takeUntil(this.ngUnsubscribe))
      .subscribe((userModel) => {
        this.listingUsers = userModel['userModel'];
      });
  }

  ngOnDestroy() {
    this.ngUnsubscribe.unsubscribe();
  }

  loadPageByNumber(page: number): Subscription {
    if (page >= 1 && page <= this.listingUsers.total_pages) {
      return this.userService
        .getUsers(page)
        .subscribe((value: UserModel) => (this.listingUsers = value));
    }

    return this.userService
      .getUsers(page)
      .subscribe((value: UserModel) => (this.listingUsers = value));
  }

  userByNameOrSurname(index: number, user: IUserDTO) {
    return user.first_name && user.last_name;
  }
}
