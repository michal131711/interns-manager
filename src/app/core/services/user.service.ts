import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../environments/environment.development';
import { Observable } from 'rxjs';
import { UserModel } from '../models/User.model';
import { UserDTO } from '../dtos/UserDTO';

@Injectable({
  providedIn: 'root',
})
export class UserService {
  constructor(private http: HttpClient) {}

  getUsers(page: number = 1): Observable<UserModel> {
    return this.http.get<UserModel>(`${environment.url}/users?page=${page}`);
  }

  getUser(userId: number): Observable<UserDTO> {
    return this.http.get<UserDTO>(`${environment.url}/users/${userId}`);
  }
}
