import { Injectable } from '@angular/core';
import { UserModel } from '../models/User.model';
import { Resolve } from '@angular/router';
import { Observable } from 'rxjs';
import { UserService } from '../services/user.service';

@Injectable()
export class UsersListResolver implements Resolve<UserModel> {
  constructor(private userService: UserService) {}

  resolve(): Observable<UserModel> | Promise<UserModel> | UserModel {
    return this.userService.getUsers(1);
  }
}
