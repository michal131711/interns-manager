import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve } from '@angular/router';
import { Observable } from 'rxjs';
import { UserService } from '../services/user.service';
import { UserDTO } from '../dtos/UserDTO';

@Injectable()
export class UserDetailsResolver implements Resolve<UserDTO> {
  constructor(private userService: UserService) {}

  resolve(
    route: ActivatedRouteSnapshot
  ): Observable<UserDTO> | Promise<UserDTO> | UserDTO {
    let userId = route.params['id'];

    return this.userService.getUser(userId);
  }
}
