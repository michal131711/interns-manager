import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AddInternComponent } from './shared/components/interns/add/add-intern.component';
import { EditInternComponent } from './shared/components/interns/edit/edit-intern.component';
import { ListInternComponent } from './core/components/interns/list/list-intern.component';
import { UsersListResolver } from './core/resolvers/users-list.resolver';
import { UserDetailsResolver } from './core/resolvers/user-details.resolver';

const routes: Routes = [
  {
    path: 'user',
    children: [
      {
        path: '',
        component: ListInternComponent,
        resolve: {
          userModel: UsersListResolver,
        },
      },
      {
        path: ':id/edit',
        component: EditInternComponent,
        resolve: {
          userDetail: UserDetailsResolver,
        },
      },
      {
        path: 'add',
        component: AddInternComponent,
      },
    ],
  },
  { path: '', redirectTo: '/user', pathMatch: 'full' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { useHash: true })],
  exports: [RouterModule],
})
export class AppRoutingModule {}
