import {
  AfterViewInit,
  Component,
  ElementRef,
  Input,
  ViewChild,
} from '@angular/core';
import * as M from 'materialize-css';
import CarouselOptions = M.CarouselOptions;
import { IUserDTO } from '../../../core/interfaces/user.interface';

@Component({
  selector: 'app-carousel',
  templateUrl: './carousel.component.html',
  styleUrls: ['./carousel.component.scss'],
})
export class CarouselComponent implements AfterViewInit {
  activeIndex: number = 0;
  prevIndex: number = 0;
  nextIndex: number = 0;
  @Input('listingUsers') listingUsers: IUserDTO[];
  @ViewChild('carousel') carousel: ElementRef<HTMLElement>;
  optionCarousel: CarouselOptions = {
    dist: -50,
    duration: 0,
    indicators: false,
    noWrap: false,
    padding: 0,
    shift: -50,
    numVisible: 3,
    fullWidth: false,
    onCycleTo: (current:Element) => {
      let children: any = current.parentElement?.children;

      for (let i = 0; i < children.length; i++) {
        const carouselItem = children[i];

        if (carouselItem.classList.contains('active')) {
          this.prevIndex = (i - 1 + children.length) % children.length;
          this.nextIndex = (i + 1) % children.length;
        }
      }
    },
  };

  ngAfterViewInit() {
    setTimeout(() => {
      M.Carousel.init(this.carousel.nativeElement, this.optionCarousel);
    });
  }
}
