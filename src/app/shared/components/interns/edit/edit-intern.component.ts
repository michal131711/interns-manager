import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { UserDTO } from '../../../../core/dtos/UserDTO';
import { Subject, takeUntil } from 'rxjs';

@Component({
  selector: 'app-edit-intern',
  templateUrl: './edit-intern.component.html',
  styleUrls: ['./edit-intern.component.scss'],
})
export class EditInternComponent implements OnInit, OnDestroy {
  private ngUnsubscribe = new Subject<UserDTO>();
  getUserData!: UserDTO;

  constructor(private readonly route: ActivatedRoute) {}

  ngOnInit() {
    this.route.data.pipe(takeUntil(this.ngUnsubscribe)).subscribe((user) => {
      this.getUserData = user['userDetail'];
    });
  }

  ngOnDestroy() {
    this.ngUnsubscribe.unsubscribe();
  }
}
